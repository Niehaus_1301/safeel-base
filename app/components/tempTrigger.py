#import app
from app import init as app

# other imports here
import time
from multiprocessing import Process

# define component


class component():
    def __init__(self, dur, dif):
        # initilization tasks here
        # set duration and difference
        self.dur = dur
        self.dif = dif

        # initialized!
        print("tempTrigger component initialized!")

    def run(self):
        # define process
        def prcs():
            # declare history array and recent temp var
            history = []
            recentTemp = 0

            while True:
                # read temp
                temp = app.temp.read()
                # append to history array
                history.append(temp)
                # round temp
                tempR = round(temp, 1)

                # if temp changed
                if (tempR != recentTemp):
                    # update recent temp
                    recentTemp = tempR
                    # run handler
                    app.handler.temp(tempR)

                # if at least DUR values captured
                if (len(history) == self.dur):
                    # calculate difference
                    difference = abs(history[0] - history[(self.dur - 1)])
                    if (difference > self.dif):
                        # run handler
                        app.handler.tempOverChange(difference)
                    # delete first history value
                    del history[0]
                time.sleep(1)

        # start process
        self.p = Process(target=prcs,)
        self.p.start()

    def stop(self):
        # stop process
        self.p.terminate()
        self.p.join()
