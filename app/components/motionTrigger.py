#import app
from app import init as app

# other imports here
from gpiozero import MotionSensor
from multiprocessing import Process


# define component
class component():
    def __init__(self):
        # initilization tasks here

        # initialized!
        print("motionTrigger component initialized!")

    def run(self):
        # define process
        def prcs():
            # init PIR
            # TODO: Figure out why PIR doesn't work if initilized outside process
            pir = MotionSensor(17)

            while True:
                # wait for PIR signal -> run handler
                pir.wait_for_motion()
                app.handler.moved(True)
                # wait for PIR timeout -> run handler
                pir.wait_for_no_motion()
                app.handler.moved(False)

        # start process
        p = Process(target=prcs,)
        p.start()

    def stop(self):
        # stop process
        self.p.terminate()
        self.p.join()
