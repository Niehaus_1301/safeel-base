#import app
from app import init as app

# other imports here
import time
from multiprocessing import Process


# define component
class component():
    def __init__(self, pin):
        # initilization tasks here
        self.pin = pin

        # initialized!
        print("door component initialized!")

    def run(self):
        # define process
        def prcs():
            # set previous status
            prev = bool(app.circuit.getStatus(self.pin))

            while True:
                # get current status
                status = bool(app.circuit.getStatus(self.pin))
                # only trigger if changed
                if not status is prev:
                    prev = status
                    # run handler
                    app.handler.doorTrigger(status)
                    time.sleep(1)

        # start process
        self.p = Process(target=prcs,)
        self.p.start()

    def stop(self):
        # stop process
        self.p.terminate()
        self.p.join()
