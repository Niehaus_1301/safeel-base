# import app
from app import init as app

# imports here
import mysql.connector


# define plugin
class plugin():
    def __init__(self, host, user, passwd, database):
        # initialization tasks here
        # set credentials
        self.db = mysql.connector.connect(
            host=host,
            user=user,
            passwd=passwd,
            database=database
        )

        # initialized!
        print("mysql Plugin Initilized")
