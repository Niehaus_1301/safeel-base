# imports here
from picamera import PiCamera
import time
import sys

# init camera
camera = PiCamera()

# get args
arg = sys.argv

# if arg is img
if arg[1] == 'img':
    # capture img
    camera.start_preview()
    time.sleep(2)
    camera.capture('/home/pi/iotpi/app/data/img/' + arg[2] + '.jpg')
    camera.stop_preview()

# if arg is video
elif arg[1] == 'video':
    # record video
    camera.start_recording('/home/pi/iotpi/app/data/video/' + arg[2] + '.h264')
    time.sleep(int(arg[3]))
    camera.stop_recording()
