# Import and initilize app
import time
from app import init as app
app.initialize()

# Other imports here

# Actions here
app.tempTrigger.run()
app.door.run()
app.motion.run()
app.objectDetection.detect()
